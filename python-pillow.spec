%global py3_incdir %(python3 -c 'import distutils.sysconfig; print(distutils.sysconfig.get_python_inc())')
 
%global with_docs 0
 
Name:           python-pillow
Version:        10.4.0
Release:        1
Summary:        Python image processing library 
License:        MIT
URL:            http://python-pillow.github.io/
Source0:       	https://files.pythonhosted.org/packages/source/p/pillow/pillow-%{version}.tar.gz

BuildRequires:  freetype-devel ghostscript lcms2-devel libimagequant-devel libjpeg-devel libtiff-devel
BuildRequires:  libwebp-devel openjpeg2-devel tk-devel zlib-devel python3-cffi python3-devel python3-numpy python3-olefile
BuildRequires:  python3-qt5 python3-setuptools python3-tkinter gcc
BuildRequires:  python3-pytest
%if %{?openEuler:1}0
BuildRequires:  libraqm-devel
%endif
%if 0%{?with_docs}
BuildRequires:  make
BuildRequires:  python3-sphinx
BuildRequires:  python3-sphinx_rtd_theme
BuildRequires:  python3-sphinx-removed-in
%endif
Requires:       ghostscript
 
%global __provides_exclude_from ^%{python3_sitearch}/PIL/.*\\.so$

%description
Pillow is the friendly PIL fork by Alex Clark and Contributors. PIL is the Python Imaging \
Library by Fredrik Lundh and Contributors. As of 2019, Pillow development is supported by Tidelift.
 
%package -n python3-pillow
Summary:        Python 3 image processing library
%{?python_provide:%python_provide python3-pillow}
Provides:       python3-imaging = %{version}-%{release}

Requires:       python3-olefile
 
%description -n python3-pillow
Pillow is the friendly PIL fork by Alex Clark and Contributors. PIL is the Python Imaging \
Library by Fredrik Lundh and Contributors. As of 2019, Pillow development is supported by Tidelift.
 
%package -n python3-pillow-devel
Summary:        Development files for pillow
Requires:       python3-devel libjpeg-devel zlib-devel python3-pillow = %{version}-%{release}

%{?python_provide:%python_provide python3-pillow-devel}

Provides:       python3-imaging-devel = %{version}-%{release}
 
%description -n python3-pillow-devel
Development files for pillow.
 
%package -n python3-pillow-help
Summary:        Documentation for pillow
BuildArch:      noarch
Requires:       python3-pillow = %{version}-%{release}
%{?python_provide:%python_provide python3-pillow-doc}
Provides:       python3-imaging-doc = %{version}-%{release} python3-pillow-doc = %{version}-%{release}

Obsoletes:	    python3-pillow-doc < %{version}-%{release}

%description -n     python3-pillow-help
Documentation for pillow.
 
 
%package -n python3-pillow-tk
Summary:        Tk interface for pillow
Requires:       python3-tkinter
Requires:       python3-pillow = %{version}-%{release}
%{?python_provide:%python_provide python3-pillow-tk}
Provides:       python3-imaging-tk = %{version}-%{release}
 
%description -n python3-pillow-tk
Tk interface for %{name}.
 
 
%package -n python3-pillow-qt
Summary:        Qt pillow image wrapper
Requires:       python3-qt5
Requires:       python3-pillow = %{version}-%{release}
%{?python_provide:%python_provide python3-pillow-qt}
Provides:       python3-imaging-qt = %{version}-%{release}
 
%description -n python3-pillow-qt
Qt pillow image wrapper.

%prep
%autosetup -p1 -n pillow-%{version} 
 
%build

%py3_build

%if 0%{?with_docs}
PYTHONPATH=$(echo $PWD/build/lib.linux-*) make -C docs html BUILDDIR=_build_py3 SPHINXBUILD=sphinx-build-%python3_version
rm -f docs/_build_py3/html/.buildinfo
%endif

%install
mkdir -p %{buildroot}/%{py3_incdir}/Imaging
install -m 644 src/libImaging/*.h %{buildroot}/%{py3_incdir}/Imaging
%py3_install
 
%check
ln -s $PWD/Images $(echo $PWD/build/lib.linux-*)/Images
cp -R $PWD/Tests $(echo $PWD/build/lib.linux-*)/Tests
cp -a $PWD/selftest.py $(echo $PWD/build/lib.linux-*)/selftest.py
pushd build/lib.linux-*
PYTHONPATH=$PWD %{__python3} selftest.py
popd
export PYTHONPATH=%{buildroot}%{python3_sitearch}
pytest --ignore=_build.python2 --ignore=_build.python3 --ignore=_build.pypy3 -v -k 'not (test_qt_image_qapplication)'
 
%files -n python3-pillow
%doc README.md CHANGES.rst
%license docs/COPYING
%{python3_sitearch}/PIL/
%{python3_sitearch}/pillow-%{version}-py%{python3_version}.egg-info
%exclude %{python3_sitearch}/PIL/_imagingtk*
%exclude %{python3_sitearch}/PIL/ImageTk*
%exclude %{python3_sitearch}/PIL/SpiderImagePlugin*
%exclude %{python3_sitearch}/PIL/ImageQt*
%exclude %{python3_sitearch}/PIL/__pycache__/ImageTk*
%exclude %{python3_sitearch}/PIL/__pycache__/SpiderImagePlugin*
%exclude %{python3_sitearch}/PIL/__pycache__/ImageQt*
 
%files -n python3-pillow-devel
%{py3_incdir}/Imaging/
 
%files -n python3-pillow-help
%if 0%{?with_docs}
%doc docs/_build_py3/html
%endif

%files -n python3-pillow-tk
%{python3_sitearch}/PIL/_imagingtk*
%{python3_sitearch}/PIL/ImageTk*
%{python3_sitearch}/PIL/SpiderImagePlugin*
%{python3_sitearch}/PIL/__pycache__/ImageTk*
%{python3_sitearch}/PIL/__pycache__/SpiderImagePlugin*
 
%files -n python3-pillow-qt
%{python3_sitearch}/PIL/ImageQt*
%{python3_sitearch}/PIL/__pycache__/ImageQt*

%changelog
* Mon Aug 19 2024 lilu <lilu@kylinos.cn> - 10.4.0-1
- Upgrade version to 10.4.0
- Raise FileNotFoundError if show_file() path does not exist
- Improved reading 16-bit TGA images with colour
- Fixed processing multiple JPEG EXIF markers

* Tue Apr 02 2024 GuoCe <guoce@kylinos.cn> - 10.3.0-1
- Upgrade version to 10.3.0

* Wed Feb 07 2024 xu_ping <707078654@qq.com> - 10.2.0-1
- Upgrade version to 10.2.0

* Wed Jan 24 2024 wangkai <13474090681@163.com> - 10.0.0-2
- Fix CVE-2023-50447

* Fri Jul 07 2023 chenzixuan <chenzixuan@kylinos.cn> - 10.0.0-1
- Fix compilation failure due to python-setuptools update to 66.0.0

* Mon May 08 2023 yaoxin <yao_xin001@hoperun.com> - 9.5.0-2
- Fix compilation failure due to python-setuptools update to 66.0.0

* Mon Apr 10 2023 yaoxin <yao_xin001@hoperun.com> - 9.5.0-1
- Update to 9.5.0

* Wed Dec 7 2022 lijian <lijian2@kylinos.cn> - 9.3.0-1
- Upgrade to 9.3.0

* Thu Nov 17 2022 qz_cx <wangqingzheng@kylinos.cn> - 9.0.1-3
- Type:CVE
- ID:NA
- SUG:NA
- DESC: fix CVE-2022-45199

* Wed Apr 20 2022 dongyuzhen <dongyuzhen@h-partners.com> - 9.0.1-2
- correct memory allocation in alloc_array (this is the rear patch of CVE-2022-22815,CVE-2022-22816)

* Thu Mar 3 2022 hanhui <hanhui15@h-partners.com> - 9.0.1-1
- DESC:update to Pillow-9.0.1

* Mon Sep 27 2021 luoyang <luoyang42@huawei.com> - 8.1.2-3
- fix CVE-2021-23437

* Thu Aug 19 2021 hanhui <hanhui15@huawei.com> - 8.1.2-2
- DESC: enable test case from setup.py

* Thu Jul 15 2021 liuyumeng <liuyumeng5@huawei.com> - 8.1.1-6
- Type:bugfix
- CVE:CVE-2021-34552
- SUG:NA
- DESC: incorporate community patch

* Wed Jul 14 2021 OpenStack_SIG <openstack@openeuler.org> - 8.1.2-1
- Update to 8.1.2

* Tue Jul 6 2021 hanhui <hanhui15@huawei.com> - 8.1.1-5
- Type:bugfix
- CVE:CVE-2021-28675 CVE-2021-28676 CVE-2021-28677 CVE-2021-28678 CVE-2021-25287 CVE-2021-25288
- SUG:NA
- DESC: incorporate community patch

* Mon Jun 21 2021 hanhui <hanhui15@huawei.com> - 8.1.1-4
- DESC: in the check section,using the cp -a instead of install

* Tue Jun 15 2021 hanhui <hanhui15@huawei.com> - 8.1.1-3
- DESC: add buildrequire gcc

* Sat Mar 13 2021 wangye <wangye70@huawei.com> - 8.1.1-2
- Type:CVE
- CVE:CVE-2021-27921 CVE-2021-27922 CVE-2021-27923
- SUG:NA
- DESC: fix CVE-2021-27921CVE-2021-27922CVE-2021-27923

* Mon Mar 08 2021 wangye <wangye70@huawei.com> - 8.1.1-1
- Update to 8.1.1
